
======
Notes.
======

Low level SD routines
=====================

Most of the code I need for SD initialisation & reading is in Sd2Card.cpp
and could be compressed down into a small library of C functions for the
bootloader.

FAT access routines.
====================

The remainder of the code is more code that I really need. I don't need
to write to the card at the moment unless I need to flag that the 
software has been uploaded.

Need to make a sketch to read the SD card and find a specific file.
